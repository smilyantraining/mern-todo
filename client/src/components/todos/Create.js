import React, { Component } from 'react';

class CreateTodo extends Component {
    state = {
        newTodo: ''
    };

    handleOnChange = (e) => {
        this.setState({
            newTodo: e.target.value
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.state.newTodo === '') {
            return;
        }
        
        this.props.onSubmit(this.state.newTodo);
        this.setState({
            newTodo: ''
        });
    }

    render() {
        return (
            <form className="add-todo" onSubmit={this.handleSubmit.bind(this)}>
                <div className="bp3-input-group bp3-large">
                    <input
                        type="text"
                        value={this.state.newTodo}
                        className="bp3-input"
                        placeholder="What do you need to do?"
                        onChange={this.handleOnChange.bind(this)} />

                    <button
                        className="bp3-button bp3-minimal bp3-intent-primary bp3-icon-plus" />
                </div>
            </form>
        );
    }
}
export default CreateTodo;