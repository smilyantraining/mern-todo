import React, { Component } from 'react';
import Todo from './Todo';
import CreateTodo from './Create';
import { TodosQuery, CreateMutation, UpdateMutation, DeleteMutation } from '../../store/todos/queries';
import { graphql, compose } from 'react-apollo';

class Todos extends Component {

    handleToggle = async (todo) => {
        await this.props.updateTodo({
            variables: {
                id: todo.id,
                complete: !todo.complete,
                text: todo.text
            },

            // Apolslo has internal cache which needs to be force-updated
            // when there are changes to state data that is already cached in Apollo (such as the state of a todo item, their number, etc.)
            update: cache => {
                let data = cache.readQuery({ query: TodosQuery });
                data.todos = data.todos.map(x => x.id === todo.id ? { ...todo, complete: !todo.complete } : x);
                cache.writeQuery({ query: TodosQuery, data });
            }
        })
    };

    handleDelete = async (todo) => {
        await this.props.deleteTodo({
            variables: {
                id: todo.id
            },
            update: cache => {
                let data = cache.readQuery({ query: TodosQuery });
                data.todos = data.todos.filter(x => x.id !== todo.id);
                cache.writeQuery({ query: TodosQuery, data });
            }
        });
    };

    handleCreateTodo = async (newTodo) => {
        await this.props.createTodo({
            variables: {
                text: newTodo
            },
            update: (cache, { data: { createTodo } }) => {
                let cachedTodos = cache.readQuery({ query: TodosQuery });
                cachedTodos.todos.unshift(createTodo);
                cache.writeQuery({ query: TodosQuery, data: cachedTodos });
            }
        });
    };

    handleEditTodo = async (todo, text) => {
        await this.props.updateTodo({
            variables: {
                id: todo.id,
                complete: todo.complete,
                text: text
            },
            update: cache => {
                let cachedTodos = cache.readQuery({ query: TodosQuery });
                cachedTodos.todos = cachedTodos.todos.map(x => x.id === todo.id ? { ...todo, complete: todo.complete, text: text } : x);
                cache.writeQuery({ query: TodosQuery, data: cachedTodos });
            }
        })
    };

    render() {
        const { data: { loading, todos } } = this.props;

        if (loading) {
            return null;
        }

        return (
            <div className="todos">
                <CreateTodo 
                    onSubmit={this.handleCreateTodo.bind(this)} />
                
                {todos.map((todo) => (
                    <Todo
                        key={`${todo.id}-todo-item`}
                        todo={todo}
                        onSaveEdit={this.handleEditTodo.bind(this, todo)}
                        onChange={this.handleToggle.bind(this, todo)}
                        onClick={this.handleDelete.bind(this, todo)} />
                ))}
            </div>
        );
    }
}

// Merging the queries to expose all of them to the <App/> component
export default compose(
    graphql(TodosQuery),
    graphql(UpdateMutation, { name: "updateTodo" }),
    graphql(DeleteMutation, { name: "deleteTodo" }),
    graphql(CreateMutation, { name: "createTodo" })
)(Todos);