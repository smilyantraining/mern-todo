import React, { Component } from 'react';
import { Card, Elevation, Checkbox, Button } from "@blueprintjs/core";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import classNames from 'classnames';

class Todo extends Component {
    state = {
        editing: false,
        text: this.props.todo.text
    }

    handleSaveEdit = (e) => {
        if (this.state.text === this.props.todo.text) {
            this.handleCancelEdit();
            return;
        }
        this.props.onSaveEdit(this.state.text);
        this.handleCancelEdit();
    }

    handleCancelEdit = (e) => {
        this.setState({
            editing: false,
            text: this.props.todo.text
        });
    }

    handleStartEdit = (e) => {
        this.setState({
            editing: true,
            text: this.props.todo.text
        });
    }

    handleEditChange = (e) => {
        if (e.key === 'Enter') {
            this.handleSaveEdit();
        }
        this.setState({
            text: e.target.value
        });
    }

    render() {
        const todo = this.props.todo;
        return (
            <div className={'todo-item ' + (todo.complete ? 'done' : 'doing')}>
                <Card
                    onDoubleClick={this.handleStartEdit.bind(this)}
                    interactive={true}
                    elevation={Elevation.ONE}>

                    <Checkbox
                        className={classNames({ 'd-none': this.state.editing })}
                        checked={todo.complete}
                        onChange={this.props.onChange.bind(this, todo)}>
                        {todo.text}
                    </Checkbox>

                    <div className={classNames('edit-todo', { 'd-none': !this.state.editing })}>
                        <input
                            type="text"
                            value={this.state.text}
                            className="bp3-input"
                            onKeyDown={this.handleEditChange.bind(this)}
                            onChange={this.handleEditChange.bind(this)} 
                            autoFocus />
                        
                        <Button
                            intent="success"
                            onClick={this.handleSaveEdit.bind(this)}>
                                <FontAwesomeIcon icon={['far', 'save']} />
                        </Button>
                        <Button
                            onClick={this.handleCancelEdit.bind(this)}>
                                <FontAwesomeIcon icon='ban' />
                        </Button>
                    </div>

                    <Button
                        intent="danger"
                        onClick={this.props.onClick.bind(this, todo)}>
                            <FontAwesomeIcon icon={['far', 'trash-alt']} />
                    </Button>
                </Card>
            </div>
        );
    }
}
export default Todo;