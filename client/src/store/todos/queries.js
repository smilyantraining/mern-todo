import gql from 'graphql-tag';

export const TodosQuery = gql`
    {
        todos {
            id
            text
            complete
        }
    }
`;

export const CreateMutation = gql`
    mutation ($text: String!) {
        createTodo(text: $text) {
            id
            text
            complete
        }
    }
`;

export const UpdateMutation = gql`
    mutation ($id: ID!, $complete: Boolean!, $text: String!) {
        updateTodo(id: $id, complete: $complete, text: $text)
    }
`;

export const DeleteMutation = gql`
    mutation ($id: ID!) {
        deleteTodo(id: $id)
    }
`;